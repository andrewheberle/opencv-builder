FROM scratch

ARG OPENCV_VERSION="4.5.2"

COPY opencv-${OPENCV_VERSION}-amd64/ /opencv/
